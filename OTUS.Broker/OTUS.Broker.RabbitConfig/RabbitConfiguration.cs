﻿using RabbitMQ.Client;

namespace OTUS.Broker.RabbitConfig
{
	public class RabbitConfiguration
	{
		private readonly IConnection _connection;
		public const string ExchangeName = "test";
		public const string QueueName = "test";
		public const string RoutingKey = "";

		public RabbitConfiguration()
		{
			_connection = GetRabbitConnection();
		}

		private IConnection GetRabbitConnection()
		{
			ConnectionFactory factory = new ConnectionFactory();
			factory.UserName = "guest";
			factory.Password = "guest";
			factory.HostName = "localhost";
			return factory.CreateConnection();
		}

		public IModel GetRabbitChannel()
		{
			IModel model = _connection.CreateModel();
			model.ExchangeDeclare(ExchangeName, ExchangeType.Fanout);
			//model.QueueDeclare(QueueName, false, false, false, null);
			//model.QueueBind(QueueName, ExchangeName, RoutingKey, null);
			return model;
		}

		public void Disconnecting()
		{
			if (_connection.IsOpen)
				_connection.Close();
		}
	}
}

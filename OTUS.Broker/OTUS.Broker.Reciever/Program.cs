﻿using System;
using System.Text;
using System.Threading;
using OTUS.Broker.RabbitConfig;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace OTUS.Broker.Reciever
{
	class Program
	{
		private static RabbitConfiguration _rabbitConfig;

		static void Main(string[] args)
		{
			Console.WriteLine("Я приемник");

			_rabbitConfig = new RabbitConfiguration();

			using (var channel = _rabbitConfig.GetRabbitChannel())
			{
				channel.ExchangeDeclareNoWait(exchange: RabbitConfiguration.ExchangeName, type: ExchangeType.Fanout);
				
				var queueName = channel.QueueDeclare("test2").QueueName;
				channel.QueueBind(queue: queueName,
								  exchange: RabbitConfiguration.ExchangeName,
								  routingKey: RabbitConfiguration.RoutingKey);

				Console.WriteLine(" [*] Ждем сообщения");

				var consumer = new EventingBasicConsumer(channel);
				consumer.Received += (model, ea) =>
				{
					var body = ea.Body.ToArray();
					var message = Encoding.UTF8.GetString(body);
					Console.WriteLine(" [x] {0}", message);
				};
				channel.BasicConsume(queue: queueName,
									 autoAck: true,
									 consumer: consumer);

				Console.WriteLine(" Press [enter] to exit.");
				Console.ReadLine();
			}

			_rabbitConfig.Disconnecting();
		}

		private static string ReceiveIndividualMessage()
		{
			string originalMessage = "";
			var model = _rabbitConfig.GetRabbitChannel();
			BasicGetResult result = model.BasicGet(RabbitConfiguration.QueueName, false);
			if (result == null)
			{
				Console.WriteLine("В настоящее время нет доступных сообщений.");
			}
			else
			{
				var body = result.Body.ToArray();
				originalMessage = Encoding.UTF8.GetString(body);
			}
			return originalMessage;
		}

	}
}

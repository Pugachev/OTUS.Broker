﻿using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using OTUS.Broker.RabbitConfig;
using System.Text;

namespace OTUS.Broker.Sender
{
	class Program
	{
		private static RabbitConfiguration _rabbitConfig;

		static void Main()
		{
			Console.WriteLine("Я отправитель");

			_rabbitConfig = new RabbitConfiguration();

			using (var channel = _rabbitConfig.GetRabbitChannel())
			{
				channel.ExchangeDeclareNoWait(RabbitConfiguration.ExchangeName, type: ExchangeType.Fanout);

				Console.WriteLine("Введите сообщение");
				var message = Console.ReadLine();
				var body = Encoding.UTF8.GetBytes(message);
				channel.BasicPublish(exchange: RabbitConfiguration.ExchangeName,
									 routingKey: RabbitConfiguration.RoutingKey,
									 basicProperties: null,
									 body: body);
				Console.WriteLine(" [x] Sent {0}", message);
			}

			Console.WriteLine(" Press [enter] to exit.");
			Console.ReadLine();
			
			_rabbitConfig.Disconnecting();
		}

	}
}
